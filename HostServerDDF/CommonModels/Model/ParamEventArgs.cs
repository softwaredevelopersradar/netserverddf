﻿using CommonModels.Struct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CommonModels.Model
{
    public class ParamEventArgs: EventArgs
    {
        public SCmdParam Param { get; private set; }

        public byte[] Data { get; private set; }
       

        public ParamEventArgs(byte channel, byte gain1, byte gain2, byte attenuator1, byte attenuator2)
        {
            Param = new SCmdParam(channel, gain1, gain2, attenuator1, attenuator2);

            Data = new byte[Marshal.SizeOf(Param)];
            Data = Serrialization.StructToByteArray(Param);

        }

        public ParamEventArgs(byte[] data)
        {
            Data = data;

            Param = new SCmdParam();
            object obj = Param;
            Serrialization.ByteArrayToStructure(Data, ref obj);
            Param = (SCmdParam)obj;

        }
    }
}
