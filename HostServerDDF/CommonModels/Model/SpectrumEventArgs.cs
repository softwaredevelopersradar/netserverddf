﻿using CommonModels.Struct;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace CommonModels.Model
{
    public class SpectrumEventArgs : EventArgs
    {

        public SCmdSpectrum Spectrum { get; private set; }

        public byte[] Data { get; private set; }


        public SpectrumEventArgs(byte channel, double frequency, byte antenna, float[] amplitude)
        {
            Spectrum = new SCmdSpectrum(channel, frequency, antenna, amplitude);

            Data = new byte[Marshal.SizeOf(Spectrum)];
            Data = Serrialization.StructToByteArray(Spectrum);

        }

        public SpectrumEventArgs(byte[] data)
        {
            Data = data;

            Spectrum = new SCmdSpectrum();
            object obj = Spectrum;
            Serrialization.ByteArrayToStructure(Data, ref obj);
            Spectrum = (SCmdSpectrum)obj;

        }
        
    }
}
