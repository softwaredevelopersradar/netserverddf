﻿using CommonModels.Struct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CommonModels.Model
{
    public class StatusRAMEventArgs:EventArgs
    {
        public SCmdStatusRAM StatusRAM { get; private set; }

        public byte[] Data { get; private set; }


        public StatusRAMEventArgs(short version, byte codeFFT, ECodeSVB codeSVB)
        {
            StatusRAM = new SCmdStatusRAM(version, codeFFT, codeSVB);

            Data = new byte[Marshal.SizeOf(StatusRAM)];
            Data = Serrialization.StructToByteArray(StatusRAM);

        }

        public StatusRAMEventArgs(byte[] data)
        {
            Data = data;

            StatusRAM = new SCmdStatusRAM();
            object obj = StatusRAM;
            Serrialization.ByteArrayToStructure(Data, ref obj);
            StatusRAM = (SCmdStatusRAM)obj;

        }
    }
}
