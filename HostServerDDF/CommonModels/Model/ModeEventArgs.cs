﻿using CommonModels.Struct;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace CommonModels.Model
{
    public class ModeEventArgs:EventArgs
    {
        public SCmdMode Mode { get; private set; }

        public byte[] Data { get; private set; }

        public ModeEventArgs(EMode mode)
        {
            Mode = new SCmdMode(mode);
           
            Data = new byte[Marshal.SizeOf(Mode)];
            Data = Serrialization.StructToByteArray(Mode);
            
        }

        public ModeEventArgs(byte[] data)
        {
            Data = data;

            Mode = new SCmdMode();
            object obj = Mode;
            Serrialization.ByteArrayToStructure(Data, ref obj);
            Mode = (SCmdMode)obj;

        }

    }
}
