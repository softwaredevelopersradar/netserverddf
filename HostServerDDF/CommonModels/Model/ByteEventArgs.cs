﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonModels.Model
{
    public class ByteEventArgs:EventArgs
    {
        public byte[] Data { get; private set; }

        public ByteEventArgs(byte[] data)
        {
            Data = data;
        }

        
    }

}
