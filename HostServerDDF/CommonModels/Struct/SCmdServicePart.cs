﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace CommonModels.Struct
{
    
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Unicode)]
    public struct SCmdServicePart
    {
        
        public SCmdServicePart(byte addressSender, byte addressReceiver,
                           ECodeDDF code , Int16 error, Int32 lenghtInform)
        {
            

            this.AddressSender = addressSender;
            this.AddressReceiver = addressReceiver;
            this.Error = error;
            this.Code = code;
            this.LenghtInform = lenghtInform;
            Data = new byte[9];


            try
            {
                Data[0] = this.AddressSender;
                Data[1] = this.AddressReceiver;
                Array.Copy(BitConverter.GetBytes(Error), 0, Data, 2, 2);
                Data[4] = this.AddressSender;
                Array.Copy(BitConverter.GetBytes(Error), 0, Data, 5, 4);
            }
            catch
            {

            }


        }

        public SCmdServicePart(byte [] data)
        {
            this.AddressSender = 0;
            this.AddressReceiver = 0;
            this.Error = 0;
            this.Code = 0;
            this.LenghtInform = 0;
            Data = data;

            try
            {
                this.AddressSender = data[0];
                this.AddressReceiver = data[1];
                this.Error = BitConverter.ToInt16(data, 2);
                this.Code = (ECodeDDF)data[4];
                this.LenghtInform = BitConverter.ToInt16(data, 5);
            }
            catch { }

        }


        public byte AddressSender { get; private set; }
        public byte AddressReceiver;
        public ECodeDDF Code;
        public Int16 Error;
        public Int32 LenghtInform;

        public byte[] Data;

    }
}
