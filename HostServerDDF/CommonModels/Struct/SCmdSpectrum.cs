﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace CommonModels.Struct
{
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Unicode)]
    public struct SCmdSpectrum
    {
        public SCmdSpectrum(byte channel,double frequency, byte antenna, float[] amplitude)
        {
            Channel = channel;

            Frequency = frequency;

            Antenna = antenna;

            Amplitude = amplitude;

            Data = new byte[2+amplitude.Length*4];

            try
            {
                Data[0] = Channel;
                Data[1] = Antenna;

                for (int i = 0; i < Amplitude.Length; i++)
                    Array.Copy(BitConverter.GetBytes(Amplitude[i]), 0, Data, 2 + i * 4, 4);
            }
            catch { }
        }

        public SCmdSpectrum(byte[] data)
        {
            Channel = 0;

            Antenna = 0;


            Frequency = 0;

            int count = (int)((data.Length - 2) / 4);

            Amplitude = new float[count];

            Data = data;

            try
            {
                Channel = Data[0];

                Frequency = BitConverter.ToDouble(Data, 1);

                Antenna = Data[1];

                
                for (int j = 0,i = 0; i < count; i += 4, j++)
                    Amplitude[j] = BitConverter.ToSingle(Data,2+i*4);
            }
            catch { }
        }

        

        public byte Channel;

        public double Frequency;

        public byte Antenna;

        public float[] Amplitude;

        public byte[] Data;
    }
}
