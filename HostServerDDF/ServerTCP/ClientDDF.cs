﻿using CommonModels.Model;
using CommonModels.Struct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ServerTCP
{
    public class ClientDDF:ClientBase
    {

        private const int LEN_HEAD = 9;

        private byte[] buffer = new byte[10000];
        private int lenght;
        private bool exist;

        public event EventHandler<ModeEventArgs> OnSetMode;
        public event EventHandler<ParamEventArgs> OnSetParam;
       


        public ClientDDF(TcpClient tcpClient)
        : base(tcpClient)
        { }

        protected async override void ReceiveMessage(byte[] data)
        {

            Array.Resize(ref buffer, lenght + data.Length);
            Array.Copy(data, 0, buffer, lenght, data.Length);

            lenght += data.Length;

            exist = true;

            while (lenght >= LEN_HEAD && exist)
            {

                byte[] head = new byte[LEN_HEAD];
                Array.Copy(buffer, 0, head, 0, LEN_HEAD);

                SCmdPatternObject cmdPatternObject = new SCmdPatternObject();
                cmdPatternObject.ServicePart = new SCmdServicePart(head);

                int iLengthCmd = cmdPatternObject.ServicePart.LenghtInform;
                if (lenght - LEN_HEAD >= iLengthCmd)
                {
                    byte[] decode = new byte[iLengthCmd];
                    Array.Copy(buffer, LEN_HEAD, decode, 0, iLengthCmd);


                    await DecodeCommand(cmdPatternObject, decode);

                    Array.Reverse(buffer);
                    Array.Resize(ref buffer, buffer.Length - (LEN_HEAD + iLengthCmd));
                    Array.Reverse(buffer);

                    lenght -= (LEN_HEAD + iLengthCmd);

                    exist = true;
                }

                else

                    exist = false;

            } // while (iTempLength > LEN_HEAD)
        }

        private async Task DecodeCommand(SCmdPatternObject CmdPatternObject, byte[] decode)
        {

            switch (CmdPatternObject.ServicePart.Code)
            {
                case ECodeDDF.SET_MODE:
                    SCmdMode sCmdMode = new SCmdMode(decode);

                    break;

                case ECodeDDF.PARAM:
                    SCmdParam sCmdParam = new SCmdParam(decode);

                    break;


                default:
                    break;



            }
        }
    }
}
