﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Storage
{
    public class Channel
    {
        public Channel()
        { }

        public Channel(ETypeChannel type)
        {
            Type = type;
        }
        public ETypeChannel Type { get; private set; }

        public double Frequency { get; set; }

        //public TParam Param { get; set; } = new TParam();

        public List<TSingleViewBand> singleViewBands { get; set; } = new List<TSingleViewBand>();
    }
}
